#!/usr/bin/env python3

import unittest
from pathlib import Path
import logging
import ast
import json

import types
import importlib.machinery
loader = importlib.machinery.SourceFileLoader('generate_repository', 'generate_repository')
generate_repository = types.ModuleType(loader.name)
loader.exec_module(generate_repository)

logging.basicConfig(level=logging.ERROR,
    format='%(levelname)8s: %(message)s')

class TestRepoGeneration(unittest.TestCase):

    helper_path = Path('tests', 'test_helpers')
    addon_path = helper_path / 'addons'

    def test_extract_blinfo_from_nonexistent(self):
        test_file = 'file_that_doesnt_exist'
        with self.assertRaises(FileNotFoundError):
            generate_repository.extract_blinfo(self.addon_path / test_file)

    def test_generate_repository_from_nonexistent(self):
        with self.assertRaises(FileNotFoundError):
            generate_repository.make_repo(Path('in_a_galaxy_far_far_away'), "somename", "someurl")

# addons which should contain bl_infos
yes_blinfo = [
        f for f in TestRepoGeneration.addon_path.iterdir()
        if not f.match('*nonaddon*') and not f.match('*invalid_addon*')
        ]
# addons which should throw BadAddon because they have no blinfo
no_blinfo = [
        f for f in TestRepoGeneration.addon_path.iterdir()
        if f.match('*nonaddon*')
        ]

def generate_good_blinfo_test(test_file: Path):
    def test(self):
        reality = generate_repository.extract_blinfo(test_file)
        with (self.helper_path / 'expected_blinfo').open("r") as f:
            expectation = ast.literal_eval(f.read())
        self.assertEqual(expectation, reality)
    return test

def generate_bad_blinfo_test(test_file: Path):
    def test(self):
        with self.assertRaises(generate_repository.BadAddon):
            generate_repository.extract_blinfo(test_file)
    return test

# Add test method retur
def add_generated_tests(test_generator, params, destclass):
    """
    Add a test method (as returned by 'test_generator') to destclass for every param
    """
    for param in params:
        test_func = test_generator(param)
        setattr(destclass, 'test_{}'.format(param), test_func)

add_generated_tests(generate_good_blinfo_test, yes_blinfo, TestRepoGeneration)
add_generated_tests(generate_bad_blinfo_test, no_blinfo, TestRepoGeneration)

