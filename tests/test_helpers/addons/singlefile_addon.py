# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8-80 compliant>

# bl_info = {
#     "name": "IvyGen",
#     "author": "testscreenings, PKHG, TrumanBlending",
#     "version": (0, 1, 2),
#     "blender": (2, 59, 0),
#     "location": "View3D > Add > Curve",
#     "description": "Adds generated ivy to a mesh object starting "
#                    "at the 3D cursor",
#     "warning": "",
#     "wiki_url": "https://wiki.blender.org/index.php/Extensions:2.6/Py/"
#                 "Scripts/Curve/Ivy_Gen",
#     "category": "Add Curve",
# }

# just use one blinfo for all addons to simplify testing
bl_info = {
    "name": "Extra Objects",
    "author": "Multiple Authors",
    "version": (0, 1, 2),
    "blender": (2, 76, 0),
    "location": "View3D > Add > Curve > Extra Objects",
    "description": "Add extra curve object types",
    "warning": "",
    "wiki_url": "https://wiki.blender.org/index.php/Extensions:2.6/Py/"
                "Scripts/Curve/Curve_Objects",
    "category": "Add Curve"
}

