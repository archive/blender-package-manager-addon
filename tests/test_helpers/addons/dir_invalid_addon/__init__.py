bl_info = {
    "author": "testscreenings, PKHG, TrumanBlending",
    "version": (0, 1, 2),
    "blender": (2, 59, 0),
    "location": "View3D > Add > Curve",
    "description": "Adds generated ivy to a mesh object starting "
                   "at the 3D cursor",
    "warning": "",
    "wiki_url": "https://wiki.blender.org/index.php/Extensions:2.6/Py/"
                "Scripts/Curve/Ivy_Gen",
    "category": "Add Curve",
}
