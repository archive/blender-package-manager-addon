# Repository generator for the blender package manager
For the package manager itself, see the `soc-2017-package_manager` branch here:
https://developer.blender.org/diffusion/B/browse/soc-2017-package_manager/

This repository contains a script (`generate_repository`) for generating
repositories of blender packages. Example usage:

	./generate_repository /path/to/packages --baseurl 'http://localhost/'

This will produce a `repo.json` file in the current directory, which can then
be copied to the server.  The baseurl is prepended to the filename of each
package to form the package's url (so for example, `http://localhost/node_wrangler.py`).

For an explanation of the other options see `generate_repository --help`:

	usage: generate_repository [-h] [-v] [-u BASEURL] [-n NAME] [-o OUTPUT] path

	Generate a blender package repository from a directory of addons

	positional arguments:
	  path                  Path to addon directory

	optional arguments:
	  -h, --help            show this help message and exit
	  -v, --verbose         Increase verbosity (can be used multiple times)
	  -u BASEURL, --baseurl BASEURL
									Component of URL leading up to the package filename.
	  -n NAME, --name NAME  Name of repo (defaults to basename of 'path')
	  -o OUTPUT, --output OUTPUT
									Directory in which to write repo.json file

